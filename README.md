# Flectra Community / hr-attendance

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[hr_attendance_report_theoretical_time](hr_attendance_report_theoretical_time/) | 2.0.1.0.5| Theoretical vs Attended Time Analysis
[hr_birthday_welcome_message](hr_birthday_welcome_message/) | 2.0.1.0.0|         This addon adds a birthday message as welcome message        when it is the employee's birthday
[hr_attendance_geolocation](hr_attendance_geolocation/) | 2.0.2.0.0|         With this module the geolocation of the user is tracked at the        check-in/check-out step
[hr_attendance_overtime](hr_attendance_overtime/) | 2.0.1.0.0| Mark Attendances as overtime.
[hr_attendance_reason](hr_attendance_reason/) | 2.0.1.1.3| HR Attendance Reason
[hr_attendance_hour_type_report](hr_attendance_hour_type_report/) | 2.0.1.0.0| HR Attendance hours report
[hr_attendance_autoclose](hr_attendance_autoclose/) | 2.0.1.1.0| Close stale Attendances
[hr_attendance_sheet](hr_attendance_sheet/) | 2.0.1.0.3| Group attendances into attendance sheets.
[hr_attendance_modification_tracking](hr_attendance_modification_tracking/) | 2.0.1.0.4|         Attendance changes will now be registered in the chatter.
[hr_attendance_rfid](hr_attendance_rfid/) | 2.0.1.1.2| HR Attendance RFID
[hr_attendance_validation](hr_attendance_validation/) | 2.0.1.0.0| Employee attendance validation.


